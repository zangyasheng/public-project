""""
作者：
功能：五角星绘制
版本：3.0
增加循环绘制不同大小的五角星
"""

import turtle
def draw_pentagram(size):
    count = 1
    while count <= 5:
        turtle.forward(size)
        turtle.right(144)
        count = count +1
def main():
    """
    主函数
    """
    turtle.penup()
    turtle.backward(100)
    turtle.pendown()
    turtle.pensize(2)
    turtle.pencolor('red')
    size = 30
    while size <= 100:
       draw_pentagram(size)
       size = size + 10
    turtle.exitonclick()

if __name__ == '__main__':
    main()
