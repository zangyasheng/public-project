"""
作者：
日期：
功能：汇率兑换
新增功能：根据输入判断是人民币还是美元，进行汇率转换
版本：2.0
"""
#汇率
USD_VS_CNY  = 6.77
#带单位的货币金额输入
currency_str_value = input("请输入带单位的货币金额：")
#获取货币单位
unit = currency_str_value[-3:]
if unit == 'CNY':
    #输入的是人民币
    #得到输入的金额字符串并转化为数字
    rmb_value = eval(currency_str_value[:-3])
    #汇率计算
    usd_value = rmb_value/USD_VS_CNY
    #输出结果
    print('美元金额是：',usd_value)
elif unit == 'USD':
    #输入的是美元
    #得到输入的金额字符串并转换为数字
    usd_value = eval(currency_str_value[:-3])
    #汇率计算
    rmb_value = usd_value * USD_VS_CNY
    #输出结果
    print('人民币金额是是：',rmb_value)
else:
    #其他货币
    print('暂不支持该种货币')