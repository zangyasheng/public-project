"""
作者：
日期：
功能：汇率兑换；根据输入判断是人民币还是美元，进行汇率转换；程序一直运行，知道用户选择退出
新增功能：将汇率兑换功能封装到函数
版本：4.0
"""
def convert_currency(im,er):
    #汇率兑换函数
    out = im * er
    return out

#汇率
USD_VS_CNY  = 6.77
#带单位的货币金额输入
currency_str_value = input('请输入带单位的货币金额(退出程序请输入Q)：')
unit = currency_str_value[-3:]
if unit == 'CNY':
    exchange_rate = 1/USD_VS_CNY
elif unit == 'USD':
    exchange_rate = USD_VS_CNY
else:
    exchange_rate = -1
while currency_str_value != 'Q':
    if exchange_rate != -1:
        in_money = eval(currency_str_value[:-3])
        # 调用函数
        out_money = convert_currency(in_money, exchange_rate)
        print('转换后的金额：', out_money)
    else:
        print('不支持该种货币！')
    currency_str_value = input('请输入带单位的货币金额(退出程序请输入Q)：')
print('正在退出程序')